﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonRegister : MonoBehaviour {

    public Button btnGC;
    public Button btnResIO;
    
    public Button btnExit;

	// Use this for initialization
	void Start () {
	    
        btnGC.onClick.AddListener( delegate(){ TestCases.Instance.CaseFunction[ECaseType.eCT_Case_GCCollect] = true; } );
        btnResIO.onClick.AddListener( delegate(){ TestCases.Instance.CaseFunction[ECaseType.eCT_Case_ResourceIO] = true; } );
        
        btnExit.onClick.AddListener( 
            delegate()
            {
                TestCases.Instance.CaseFunction[ECaseType.eCT_Case_GCCollect] = false; 
                TestCases.Instance.CaseFunction[ECaseType.eCT_Case_MonoBehavior] = false; 
                TestCases.Instance.CaseFunction[ECaseType.eCT_Case_ResourceIO] = false; 
                TestCases.Instance.CaseFunction[ECaseType.eCT_HardwareResource] = false;                
             } 
        );

        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
