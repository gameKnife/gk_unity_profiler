﻿using UnityEngine;
using System.Collections;

public class MonoCoster : MonoBehaviour {

    public int coster = 0;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
        // I'm Costing time!

        nProfiler.np_begin_event(PE.FrameNetwork);
        for (int i=0; i < Random.Range(5000, 99999); ++i)
        {
            coster++; 
        }
        nProfiler.np_end_event(PE.FrameNetwork);

        nProfiler.np_begin_event(PE.FrameAction);
        for (int i=0; i < Random.Range(1, 99); ++i)
        {
            StartCoroutine(AsyncCost());
        }
        nProfiler.np_end_event(PE.FrameAction);
    }

    IEnumerator AsyncCost()
    {
        coster++;
        yield return null;
        coster++;
    }
}
