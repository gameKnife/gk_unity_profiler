﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public enum ECaseType
{
    eCT_Case_GCCollect,
    eCT_Case_MonoBehavior,
    eCT_Case_ResourceIO,
    eCT_HardwareResource,
    eCT_SceneLoad,
    eCT_SceneLoad_ActorLoad,
    eCT_SceneLoad_ActorLoad_MonoCost,
    eCT_None,
}

public class TestCases : MonoBehaviour {
    
    static TestCases instance;
    public static TestCases Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<TestCases>();
            }
            return instance;            
        }
    }
    int current_case = 0;
    
    float timer = 0;
    
    public ECaseType autoRun = ECaseType.eCT_None;
    
    public Dictionary<ECaseType, bool> CaseFunction = new Dictionary<ECaseType, bool>();
    
    void Start()
    {
        Application.targetFrameRate = -1;

        DontDestroyOnLoad( this );
        
        timer = UnityEngine.Time.realtimeSinceStartup;
        
        foreach( ECaseType suit in System.Enum.GetValues(typeof(ECaseType)) )
        {
            CaseFunction.Add(suit, false);
        }
        
        if(autoRun != ECaseType.eCT_None)
        {
            CaseFunction[autoRun] = true;
        }

        if (CaseFunction[ECaseType.eCT_SceneLoad_ActorLoad_MonoCost])
        {
            Object asset = Resources.Load("MonoCoster");
            for (int i=0; i < 100; ++i)
            {
                GameObject.Instantiate(asset);
            }
        }
    }
    
    void Update()
    {
        nProfiler.np_begin_event(PE.FrameAppRoot);
        if( UnityEngine.Time.realtimeSinceStartup - timer > 4.0f )
        {
            timer = UnityEngine.Time.realtimeSinceStartup;
            
            HeartBeat();
        }
        nProfiler.np_end_event(PE.FrameAppRoot);
    }
    
    void HeartBeat()
    {
        if( CaseFunction[ECaseType.eCT_Case_GCCollect] )
        {
            GCCollect();
        }
        if( CaseFunction[ECaseType.eCT_Case_MonoBehavior] )
        {
            MonoBehaviour();
        }
        if( CaseFunction[ECaseType.eCT_Case_ResourceIO] )
        {
            ResourceIO();
        }
        if( CaseFunction[ECaseType.eCT_HardwareResource] )
        {
            ResourceIO();
        }
        if( CaseFunction[ECaseType.eCT_SceneLoad] )
        {
            SceneLoad(false);
        }
        if( CaseFunction[ECaseType.eCT_SceneLoad_ActorLoad] )
        {
            SceneLoad(true);
        }
        if (CaseFunction[ECaseType.eCT_SceneLoad_ActorLoad_MonoCost])
        {
            SceneLoad(true);
        }
    }
    
    
    bool loaded = false;
    public void SceneLoad(bool withPlayer)
    {
        if(!loaded)
        {
            
            Debug.Log("In case of shader compile spend, we should add all shader in use into ProjectSetting/Graphics, and Warmup Here.");
            float time = Time.realtimeSinceStartup;
            Shader.WarmupAllShaders();
            time = Time.realtimeSinceStartup - time;
            Debug.Log(string.Format("Shader warmup finised. using {0:F2}ms", time * 1000.0f) );
            
            StartCoroutine( LoadSceneAsync("OPEN_004A", withPlayer) );
            
            loaded = true;
            
        }       
    }
    
    public IEnumerator LoadSceneAsync( string levelName, bool withPlayer )
    {
        //nProfiler.np_begin_report(PM.Loading);
        nProfiler.NP_START_REPORT(PM.Loading);

        nProfiler.np_begin_event(PE.LoadingScene);

        {
            nProfiler.np_begin_event(PE.LoadingGeometry);

            AsyncOperation ao = Application.LoadLevelAsync( levelName );
            yield return ao;

            nProfiler.np_end_event(PE.LoadingGeometry);
        }

        
        if( withPlayer )
        {
            nProfiler.np_begin_event(PE.LoadingActor);

            GameObject asset = Resources.Load( "Player/PM_01" ) as GameObject;
            if( asset != null )
            {
                GameObject player = Instantiate(asset, new Vector3(37, 4, 0), Quaternion.identity) as GameObject;
            }

            nProfiler.np_end_event(PE.LoadingActor);

            for (int i=0; i < 1000; ++i)
            {
                nProfiler.np_begin_event(PE.LoadingEffect);

                for(int k=0; k < 100000; ++k)
                {
                    
                }

                nProfiler.np_end_event(PE.LoadingEffect);
            }
        }

        nProfiler.np_end_event(PE.LoadingScene);

        nProfiler.NP_END_REPORT(PM.Loading);

//         nProfiler.np_end_report(PM.Loading);
//         StringBuilder sb = new StringBuilder(1024);
//         nProfiler.np_get_report(PM.Loading, sb, 1024);
//         Debug.Log(sb.ToString());
    }
    
    bool GCAllocated = false;
    public void GCCollect()
    {
        if(GCAllocated)
        {
            System.GC.Collect();
            GCAllocated = false;
            Debug.Log("GCCollected.");
        }
        else
        {
            int[] data = new int[ 8 * 1024 * 1024];// 8mb
            GCAllocated = true;
            Debug.Log("Allocate 8MB.");
        }
    }
    
    public void MonoBehaviour()
    {
        
    }
    
    int step = 0;
    public RawImage texHolder = null;
    public GameObject meshHolder = null;
    public void ResourceIO()
    {  
        switch( step )
        {
            case 0:
            {
                Texture2D tex = Resources.Load("LoadTexture/normal512") as Texture2D;
                
                if(texHolder != null)
                {
                    texHolder.texture = tex;
                }
                
                Debug.Log("Tex Loaded.");
            }
            break;
            case 1:
            {
                if(texHolder != null)
                {
                    texHolder.texture = null;
                }
                
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                
                Debug.Log("Tex Unload.");
            }       
            break;
            case 2:
            {
                GameObject go = Resources.Load("LoadMesh/test1MB") as GameObject;
                meshHolder = Instantiate( go ) as GameObject;
                
                Debug.Log("MeshR/W Load.");
            }
            break;
            case 3:
            {
                if( meshHolder != null)
                {
                    DestroyImmediate(meshHolder);
                    meshHolder = null;
                }
                
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                
                Debug.Log("MeshR/W Unload.");
            }
            break;
            case 4:
            {
                GameObject go = Resources.Load("LoadMesh/test1MBReadOnly") as GameObject;
                meshHolder = Instantiate( go ) as GameObject;
                
                Debug.Log("MeshW Load.");
            }
            break;
            case 5:
            {
                if( meshHolder != null)
                {
                    DestroyImmediate(meshHolder);
                    meshHolder = null;
                }
                
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                
                Debug.Log("MeshW Unload.");
            }
            break;
            case 6:
            {
                GameObject go = Resources.Load("LoadMesh/test1MB") as GameObject;
                meshHolder = Instantiate( go ) as GameObject;
                
                // modify meshfilter
                MeshFilter mf = meshHolder.GetComponentInChildren<MeshFilter>();
                if(mf != null )
                {
                    mf.sharedMesh.UploadMeshData(true);
                }       
                
                Debug.Log("MeshR/W Clean Load.");
            }
            break;
            case 7:
            {
                if( meshHolder != null)
                {
                    DestroyImmediate(meshHolder);
                    meshHolder = null;
                }
                
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                
                Debug.Log("MeshR/W Clean Unload.");
            }
            break;
        }

        step++;
        step %= 8;
    }
}
