﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class BuildTool {


    static string[] GetActiveScene()
    {
        List<string> gatherScns = new List<string>();
        foreach( EditorBuildSettingsScene scn in EditorBuildSettings.scenes )
        {
            if( scn.enabled )
            {
                gatherScns.Add( scn.path );
            }
        } 
        return gatherScns.ToArray();
    }

    [MenuItem("Build/iOS")]
    public static void BuildiOS()
    {    
        BuildPipeline.BuildPlayer( GetActiveScene(), "Build" , BuildTarget.iPhone, BuildOptions.None );
    }
    
    [MenuItem("Build/iOSDebug")]
    public static void BuildiOSDebug()
    {    
        BuildPipeline.BuildPlayer( GetActiveScene(), "Build" , BuildTarget.iPhone, BuildOptions.Development );
    }

}
