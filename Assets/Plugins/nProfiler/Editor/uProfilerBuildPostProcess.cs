using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
#endif
using System.IO;

public static class uProfilerBuildPostProcess
{

#if UNITY_EDITOR
        public static void ReplaceSystemFile(string projectRootPath, string filename)
        {
            string deleteFilePath = string.Format("{0}/Classes/{1}", projectRootPath, filename);
            Debug.Log(deleteFilePath);
            if( File.Exists(deleteFilePath) )
            {
                string[] files = Directory.GetFiles( Application.dataPath, filename, SearchOption.AllDirectories );
                foreach( string file in files ) {
                    UnityEngine.Debug.Log("Replace File: "+filename);
                    File.Delete(deleteFilePath);
                    File.Copy( file, deleteFilePath );
                }
            }
        }
        
	[PostProcessBuild(998)]
	public static void OnPostProcessBuild( BuildTarget target, string pathToBuiltProject )
	{
		if (target != BuildTarget.iPhone) {
			Debug.LogWarning("Target is not iPhone. XCodePostProcess will not run");
			return;
		}
        
        Debug.Log("uProfiler Build PostProcess");

        ReplaceSystemFile( pathToBuiltProject, "iPhone_Profiler.h" );
        ReplaceSystemFile( pathToBuiltProject, "iPhone_Profiler.cpp" );

	}
#endif

	public static void Log(string message)
	{
		UnityEngine.Debug.Log("PostProcess: "+message);
	}
}
