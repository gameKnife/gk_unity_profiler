﻿using UnityEngine;
using System.Collections;


public static class IPM
{
    public const ushort FrameProfile = 1000;
}

public static class IPE
{
    public const ushort FrameTimeTotal = 10001;

    public const ushort FrameGPU =      19001;
    public const ushort FrameMono =     19002;
    public const ushort FrameMonoFixed = 19003;
    public const ushort FramePYX =      19004;
    public const ushort FrameSKN =      19005;
    public const ushort FrameANI =      19006;
    public const ushort FrameBTC =      19007;
    public const ushort FrameCUL =      19008;
}

/// <summary>
/// Profile Module
/// </summary>
public static class PM
{
    public const ushort Loading = 0;
}

/// <summary>
/// ProfileEntry
/// </summary>
public static class PE
{
    public const ushort LoadingScene = 0;
    public const ushort LoadingGeometry = 1;
    public const ushort LoadingActor = 2;
    public const ushort LoadingEffect = 3;


    public const ushort FrameAppRoot = 1001;
    public const ushort FrameNetwork = 1002;
    public const ushort FrameAction = 1003;


    
}

public class nProfilerConfig {

	public static void Setup () {

        nProfiler.np_set_module_name(PM.Loading, "LoadingTime");
        nProfiler.np_set_module_name(IPM.FrameProfile, "FrameCost");

        nProfiler.np_set_event_name(IPE.FrameTimeTotal, IPM.FrameProfile, "TotalFrameTime");

        nProfiler.np_set_event_name(IPE.FrameGPU, IPM.FrameProfile, "  GPU");
        nProfiler.np_set_event_name(IPE.FrameMono, IPM.FrameProfile, "  UPD");
        nProfiler.np_set_event_name(IPE.FrameMonoFixed, IPM.FrameProfile, "  FIX");
        nProfiler.np_set_event_name(IPE.FramePYX, IPM.FrameProfile, "  PYX");
        nProfiler.np_set_event_name(IPE.FrameSKN, IPM.FrameProfile, "  SKN");
        nProfiler.np_set_event_name(IPE.FrameANI, IPM.FrameProfile, "  ANI");
        nProfiler.np_set_event_name(IPE.FrameBTC, IPM.FrameProfile, "  BTC");
        nProfiler.np_set_event_name(IPE.FrameCUL, IPM.FrameProfile, "  CUL");

        nProfiler.np_set_event_name(PE.LoadingScene, PM.Loading, "Load_Scene");
        nProfiler.np_set_event_name(PE.LoadingGeometry, PM.Loading, "  Load_Geometry");
        nProfiler.np_set_event_name(PE.LoadingActor, PM.Loading, "  Load_Actor");
        nProfiler.np_set_event_name(PE.LoadingEffect, PM.Loading, "  Load_Effect");
       
        nProfiler.np_set_event_name(PE.FrameAppRoot, PM.Loading, "  AppRoot");
        nProfiler.np_set_event_name(PE.FrameNetwork, PM.Loading, "  Network");
        nProfiler.np_set_event_name(PE.FrameAction, PM.Loading, "  Action");

    }
	
}
