﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;

#if UNITY_EDITOR
using UnityEditor;
#endif

[StructLayout(LayoutKind.Sequential)]
public struct nProfileElement
{
    public ushort id;
    public ushort moduleId;

    public ulong durationInTick;
    public ulong lastStartTime;

    public uint callCount;
};

public class nProfiler
{ 
#if UNITY_EDITOR
    const string dllname = "nProfiler";
#elif UNITY_ANDROID
    const string dllname = "nProfiler";
#else
    const string dllname = "__Internal";
#endif
    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_init(string str);
    
    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_shutdown();

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_begin_event(ushort id);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_end_event(ushort id);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_get_event(ushort id, ref nProfileElement outData);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_set_event(ushort id, uint timeInTick, uint count);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_begin_report(ushort moduleid);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_end_report(ushort moduleid);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_get_report(ushort moduleid, StringBuilder target, int len);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_get_report_count(ushort moduleid, ref int count);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_get_report_by_idx(uint idx, ushort moduleid, StringBuilder target, int len);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_set_event_name(ushort id, ushort moduleid, string str);

    [System.Diagnostics.Conditional("NPROFILE")]
    [DllImport(dllname)]
    static public extern void np_set_module_name(ushort id, string str);

    [System.Diagnostics.Conditional("NPROFILE")]
    static public void NP_START_REPORT(ushort moduleid)
    {
        np_begin_report(moduleid);
    }

    [System.Diagnostics.Conditional("NPROFILE")]
    static public void NP_END_REPORT(ushort moduleid)
    {
        np_end_report(moduleid);

        if(moduleid == IPM.FrameProfile)
        {
            // only if frame time > 33ms, lower 30FPS, fucked out the report
//             nProfileElement npe = new nProfileElement();
//             nProfiler.np_get_event(IPE.FrameTimeTotal, ref npe);
//             if (npe.durationInTick > 33)
//             {
//                 StringBuilder sb = new StringBuilder(2048);
//                 nProfiler.np_get_report(moduleid, sb, 2048);
// 
//                 nProfilerSystem.Instance.ShowReport(5.0f, "" + moduleid, sb.ToString());
//                 Debug.Log(string.Format("<color=brown><b>Time Report of {0}</b></color>\n{1}", moduleid, sb.ToString()));
//             }
        }
        else
        {
            StringBuilder sb = new StringBuilder(2048);
            nProfiler.np_get_report(moduleid, sb, 2048);

            nProfilerSystem.Instance.ShowReport(5.0f, "" + moduleid, sb.ToString());
            Debug.Log(string.Format("<color=brown><b>Time Report of {0}</b></color>\n{1}", moduleid, sb.ToString()));
        }



    }
}

[StructLayout(LayoutKind.Sequential)]
public struct uProfileElement
{
    public float min;
    public float max;
    public float avg;
};

[StructLayout(LayoutKind.Sequential)]
public struct uProfileInfo
{
    public uProfileElement frametime;
     
    public uProfileElement cpu_player;
    public uProfileElement cpu_gfx;
    public uProfileElement cpu_swap;
    
    public uProfileElement gpu;
    
    public uProfileElement drawcall;
    public uProfileElement tris;
    public uProfileElement verts;
    
    public uProfileElement detail_physx;
    public uProfileElement detail_anim;
    public uProfileElement detail_cull;
    public uProfileElement detail_skin;
    public uProfileElement detail_batching;
    public uProfileElement detail_render;
    
    public uProfileElement script_update;
    public uProfileElement script_fixedupdate;
    public uProfileElement script_coroutines;
};

public static class uProfileUtils
{
    public static uint GetMonoHeapSize()
    {
#if UNITY_EDITOR       
        return (uint)Profiler.GetMonoHeapSize();
#else      
        return 0;//(uint)mono_gc_get_heap_size();
#endif
    }
   
    public static uint GetMonoUsedSize()
    {
#if UNITY_EDITOR
        return(uint )Profiler.GetMonoUsedSize();
#else
        return 0;//(uint)mono_gc_get_used_size();
#endif     
    }
    
    public static uint GetTotalAllocated()
    {
#if UNITY_EDITOR
        return(uint )(Profiler.GetTotalAllocatedMemory() + Profiler.GetTotalUnusedReservedMemory());
#else
        return (uint)np_get_total_allocate();
#endif     
    }
    
    public static void GetProfileInfo(ref uProfileInfo info)
    {
#if UNITY_EDITOR
        info.drawcall.avg = (float)UnityEditor.UnityStats.drawCalls;
        info.frametime.avg = Time.unscaledDeltaTime * 1000.0f;
#elif UNITY_ANDROID

#else
        gp_get_profile_info(ref info); 
#endif
    }

    #region NativeInterface

    // #if UNITY_IPHONE && !UNITY_EDITOR
    //     [DllImport("__Internal")]
    //     static extern System.Int64 mono_gc_get_heap_size();
    // #endif 

    // #if UNITY_IPHONE && !UNITY_EDITOR
    //     [DllImport("__Internal")]
    //     static extern System.Int64 mono_gc_get_used_size();
    // #endif

#if (UNITY_IPHONE && !UNITY_EDITOR) || UNITY_ANDROID
    [DllImport("__Internal")]
    static extern int np_get_total_allocate();
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
    [DllImport("__Internal")]
    static extern void gp_get_profile_info(ref uProfileInfo info);
#endif

    #endregion
}



public class nProfilerSystem : MonoBehaviour {

    static nProfilerSystem instance;
    static public nProfilerSystem Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<nProfilerSystem>();
            }
            return instance;
        }
    }

    uint mono_heap;
    uint mono_heap_used;    
    uint total_allocated;
    
    public Text loginfo;
    public GameObject ReportPanel;
    public Text ReportTitle;
    public Text ReportText;
    public Button ReportButton;

    public bool recordFrame = false;

    uProfileInfo profInfo = new uProfileInfo();

	// Use this for initialization

	void Awake ()
    {

	    DontDestroyOnLoad(this);

        nProfiler.np_init(Application.persistentDataPath);

        nProfilerConfig.Setup();

        onCloseReportPanel();

        Profiler.enabled = true;
    }
    
    void OnDestroy()
    {
        nProfiler.np_shutdown();
    }
	
	// Update is called once per frame
	void Update () {
	    
        if(loginfo != null)
        {
            // Unity Integrated Profile Infos
            uProfileUtils.GetProfileInfo(ref profInfo);

            mono_heap = uProfileUtils.GetMonoHeapSize();
            mono_heap_used = uProfileUtils.GetMonoUsedSize();
            total_allocated = uProfileUtils.GetTotalAllocated();


            string upInfo = string.Format("Mono: {0:F2}MB / {1:F2}MB) | Total: {2:F2}MB\n"
            + "FrameTime: {3:F2}ms | GPU: {4:F2}ms | MonoUpdate: {5:F2}ms | PYX: {6:F2}ms | SKN: {7:F2}ms | ANI: {8:F2}ms | BTC: {9:F2}ms | CUL: {10:F2}ms\n"
            + "DP: {11}, Tri: {12}, Vert: {13}",
            (float)mono_heap_used / (1024.0f * 1024.0f),
            (float)mono_heap / (1024.0f * 1024.0f),
            (float)total_allocated / (1024.0f * 1024.0f),

            profInfo.frametime.avg,
            profInfo.gpu.avg,
            profInfo.script_update.avg,

            profInfo.detail_physx.avg,
            profInfo.detail_skin.avg,
            profInfo.detail_anim.avg,

            profInfo.detail_batching.avg,
            profInfo.detail_cull.avg,

            (int)profInfo.drawcall.avg,
            (int)profInfo.tris.avg,
            (int)profInfo.verts.avg
            );

            loginfo.text = upInfo;


            // directly set uProfile info into nProfile, to generate report
            nProfiler.np_set_event(IPE.FrameGPU, (uint)profInfo.gpu.avg, 1);
            nProfiler.np_set_event(IPE.FrameMono, (uint)profInfo.script_update.avg, 1);
            nProfiler.np_set_event(IPE.FrameMonoFixed, (uint)profInfo.script_fixedupdate.avg, 1);
            nProfiler.np_set_event(IPE.FramePYX, (uint)profInfo.detail_physx.avg, 1);
            nProfiler.np_set_event(IPE.FrameSKN, (uint)profInfo.detail_skin.avg, 1);
            nProfiler.np_set_event(IPE.FrameANI, (uint)profInfo.detail_anim.avg, 1);
            nProfiler.np_set_event(IPE.FrameBTC, (uint)profInfo.detail_batching.avg, 1);
            nProfiler.np_set_event(IPE.FrameCUL, (uint)profInfo.detail_cull.avg, 1);
        }



        // nProfiler datas
        nProfiler.np_end_event(IPE.FrameTimeTotal);
        if(recordFrame)
        {
            nProfiler.NP_END_REPORT(IPM.FrameProfile);
            nProfiler.NP_START_REPORT(IPM.FrameProfile);
        }
        nProfiler.np_begin_event(IPE.FrameTimeTotal);
    }

    public void ShowReport(float second, string moduleName, string content)
    {
        StartCoroutine(ShowReportAsync(second, moduleName, content));
    }

    IEnumerator ShowReportAsync(float second, string moduleName, string content)
    {
        onOpenReportPanel(moduleName, content);

        yield return new WaitForSeconds( second );

        onCloseReportPanel();
    }

    void onOpenReportPanel(string moduleName, string content)
    {
        if (ReportPanel != null)
        {
            ReportPanel.SetActive(true);
            ReportTitle.text = string.Format("Report of {0}", moduleName);
            ReportText.text = content;
        }
    }

    void onCloseReportPanel()
    {
        if( ReportPanel != null )
        {
            ReportPanel.SetActive(false);
        }
    }

    public void onSwitchRecord(bool check)
    {
        if (ReportPanel != null)
        {
            recordFrame = check;
        }
    }

    public void onSwitchReportPanel()
    {
        if (ReportPanel != null)
        {
            if( ReportPanel.activeSelf )
            {
                ReportPanel.SetActive(false);
            }
            else
            {
                ReportPanel.SetActive(true);
            }
        }
    }

    public int currReportIdx = 0;

    private void FlushReport()
    {
        if (ReportPanel != null)
        {
            int count = 0;
            nProfiler.np_get_report_count(IPM.FrameProfile, ref count);

            if (count > 0)
            {
                currReportIdx = Mathf.Clamp(currReportIdx, 0, count);

                StringBuilder sb = new StringBuilder(1000);
                nProfiler.np_get_report_by_idx((uint)currReportIdx, IPM.FrameProfile, sb, 1000);
                ReportTitle.text = string.Format("Time of Frame {0}/{1}", currReportIdx, count);
                ReportText.text = sb.ToString();
            }
        }
    }

    public void onSwitchPrevReport(int step)
    {
        currReportIdx-=step;
        FlushReport();

    }

    public void onSwitchNextReport(int step)
    {
        currReportIdx+=step;
        FlushReport();
    }

    public void onSaveReport()
    {

    }

    public void onOpenReport()
    {

    }
}
