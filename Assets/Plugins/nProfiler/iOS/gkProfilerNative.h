

#if defined (__cplusplus)
extern "C"
{
#endif
    int gp_get_total_allocate();
    int gp_get_total_free();
#if defined (__cplusplus)
}
#endif

