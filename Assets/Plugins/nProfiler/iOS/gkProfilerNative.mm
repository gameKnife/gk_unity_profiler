#import <mach/mach.h>
#import <mach/mach_host.h>

void internal_get_memory(int& used, int& free)
{
//    mach_port_t host_port;
//    mach_msg_type_number_t host_size;
//    vm_size_t pagesize;
//    
//    host_port = mach_host_self();
//    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
//    host_page_size(host_port, &pagesize);
//    
//    vm_statistics_data_t vm_stat;
//    
//    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
//        NSLog(@"Failed to fetch vm statistics");
//    
//    /* Stats in bytes */
//    // Calculating total and used just to show all available functionality
//    natural_t mem_used = (vm_stat.active_count +
//                          vm_stat.inactive_count +
//                          vm_stat.wire_count) * pagesize;
//    natural_t mem_free = vm_stat.free_count * pagesize;
//    natural_t mem_total = mem_used + mem_free;
    
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        used = (int)info.resident_size;
    } else {
        used = (int)0;
    }
    
    free = 0;
}

extern "C"
{
    
    
    int gp_get_total_allocate()
    {
        int mem_used = 0;
        int mem_freed = 0;
        
        internal_get_memory(mem_used, mem_freed);
        
        return (int) mem_used;
    }
    
    int gp_get_total_free()
    {
        int mem_used = 0;
        int mem_freed = 0;
        
        internal_get_memory(mem_used, mem_freed);
        
        return (int) mem_freed;
    }
    
}