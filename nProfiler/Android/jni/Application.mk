APP_ABI := armeabi armeabi-v7a

APP_OPTIM := release

APP_PLATFORM := android-9

APP_STL := gnustl_static

APP_CPPFLAGS := -frtti -fexceptions -Wno-psabi -Wno-error=format-security -fpermissive -DANDROID -DCORE_EXPORTS

APP_MODULES := nProfiler

