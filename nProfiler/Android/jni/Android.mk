#############################################
# module : nProfiler
# desc : 
#############################################

LOCAL_PATH := $(call my-dir)

#############################################
# module : include lib
#############################################
#include $(CLEAR_VARS)
#LOCAL_MODULE := preb_vfs
#LOCAL_SRC_FILES := ../../../../../bin/android/$(TARGET_ARCH_ABI)/libvfs.a
#include $(PREBUILT_STATIC_LIBRARY)


MODULE_nProfiler_SRC_FILES := \
	../../nProfiler/interface.cpp \
	../../nProfiler/nProfiler.cpp

MODULE_nProfiler_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../../ \
	$(LOCAL_PATH)/../../../ 

MODULE_nProfiler_LDLIBS := \
#	-llog \
#	-lz \
#	-lGLESv2 \
	-lc

MODULE_nProfiler_STATIC_LIBS := \
#	preb_ext2_uuid

MODULE_nProfiler_WHOLE_STATIC_LIBS := \
#	preb_ext2_uuid

MODULE_nProfiler_CPP_FEATURES := \
#	rtti \
#	exception

MODULE_nProfiler_C_FLAGS := \
	-W -O3 \
	-Wno-psabi \
	-fpermissive \
	-DANDROID \
	-std=gnu++11

#############################################
# shared module : core
#############################################
include $(CLEAR_VARS)

LOCAL_MODULE := nProfiler

LOCAL_SRC_FILES := $(MODULE_nProfiler_SRC_FILES)

LOCAL_EXPORT_C_INCLUDES := $(MODULE_nProfiler_INCLUDES)

LOCAL_C_INCLUDES := $(MODULE_nProfiler_INCLUDES)

LOCAL_LDLIBS := $(MODULE_nProfiler_LDLIBS)

LOCAL_EXPORT_LDLIBS := $(MODULE_nProfiler_LDLIBS)

LOCAL_STATIC_LIBRARIES := $(MODULE_nProfiler_STATIC_LIBS)

LOCAL_WHOLE_STATIC_LIBRARIES := $(MODULE_nProfiler_WHOLE_STATIC_LIBS)

LOCAL_CPP_FEATURES := $(MODULE_nProfiler_CPP_FEATURES)

LOCAL_CFLAGS := $(MODULE_nProfiler_C_FLAGS)

LOCAL_CPPFLAGS := $(MODULE_nProfiler_C_FLAGS)

LOCAL_EXPORT_CFLAGS := $(MODULE_nProfiler_C_FLAGS)

LOCAL_EXPORT_CPPFLAGS := $(MODULE_nProfiler_C_FLAGS)

LOCAL_ARM_MODE := arm

TARGET_ARCH := arm

LOCAL_ALLOW_UNDEFINED_SYMBOLS := false

include $(BUILD_SHARED_LIBRARY)

