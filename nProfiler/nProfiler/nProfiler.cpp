//
//  nProfiler.m
//  nProfiler
//
//  Created by gameKnife on 16/3/30.
//  Copyright © 2016年 gameKnife. All rights reserved.
//

#include "nProfiler.h"
#include "timer.h"

namespace NPR {

	nProfiler::nProfiler(const char* datapath)
	{
		mElements.assign(MAX_UINT16, (nProfileEvent*)NULL);
		mEventNames.assign(MAX_UINT16, std::string());
		mModuleNames.assign(MAX_UINT16, std::string());
		mModuleReports.assign(MAX_UINT16, std::vector< nProfilerSnapshot >());

		mDataPath = datapath;

        npTimer::init();
	}

	nProfiler::~nProfiler()
	{
		for (auto itor = mElements.begin(); itor != mElements.end(); ++itor)
		{
			if ( *itor != NULL)
			{
				delete (*itor);
			}
		}
	}

	void nProfiler::BeginEvent(uint16 id)
	{
		mElements[id]->Begin();
	}

	void nProfiler::EndEvent(uint16 id)
	{
		mElements[id]->End();
	}

	void nProfiler::SetEvent(uint16 id, uint32 timeInTick, uint32 count)
	{
		mElements[id]->durationInTick = (uint64_t)timeInTick;
		mElements[id]->callCount = count;
	}

	void nProfiler::GetEvent(uint16 id, nProfileEvent& out)
	{
		out = *(mElements[id]);
	}

	void nProfiler::BeginReport(uint16 id)
	{
		for (auto itor = mElements.begin(); itor != mElements.end(); ++itor)
		{
			if (*itor != NULL && (*itor)->moduleId == id)
			{
				(*itor)->Reset();
			}
		}
	}

	void nProfiler::EndReport(uint16 id)
	{
		nProfilerSnapshot snapshot;

		for (auto itor = mElements.begin(); itor != mElements.end(); ++itor)
		{
			if (*itor != NULL && (*itor)->moduleId == id)
			{
				snapshot.AddEvent(*(*itor));
			}
		}

		// RECORDING
		// mModuleReports[id].push_back(snapshot);

		// REPLACE
		//mModuleReports[id].clear();
		mModuleReports[id].push_back(snapshot);

		// log may
	}

	void nProfiler::GetReportString(uint16 id, char* target, int len)
	{
		if (mModuleReports[id].size() > 0)
		{
			GetReportStringByIdx(mModuleReports[id].size() - 1, id, target, len);
		}
	}

	void nProfiler::GetReportStringByIdx(uint32 idx, uint16 moduleid, char* target, int len)
	{
		if (idx < mModuleReports[moduleid].size())
		{
			nProfilerSnapshot& snapshot = mModuleReports[moduleid][idx];

			std::string report;

			int len_use = 0;
			for (auto unit = snapshot.units.begin(); unit != snapshot.units.end(); ++unit)
			{
				char buff[512];
				sprintf(buff, "%s call: %d time: %dms\n", mEventNames[unit->eventid].c_str(), unit->callCount, unit->durationInTick);

				len_use += strlen(buff);

				if (len_use < len)
				{
					strcat(target, buff);
				}
			}
		}
	}

	uint32 nProfiler::GetReportCount(uint16 moduleid)
	{
		return mModuleReports[moduleid].size();
	}

	void nProfiler::SetEventName(uint16 id, uint16 moduleid, const char* str)
	{
		if (mElements[id] == NULL)
		{
			mElements[id] = new nProfileEvent(id, moduleid);
		}
		mEventNames[id] = str;
	}

	void nProfiler::SetModuleName(uint16 id, const char* str)
	{
		mModuleNames[id] = str;
	}

	void nProfiler::ExportReportRecord(uint16 moduleid, const char* destAbs)
	{
		
	}

	void nProfiler::LogReport(uint16 id)
	{
		if ( mModuleReports[id].size() > 0  )
		{
			nProfilerSnapshot& snapshotRef = mModuleReports[id][0];

			char outputPath[255];
			sprintf(outputPath, "%s/%s%d.npr", mDataPath.c_str(), "module", id);

			FILE* fp = fopen(outputPath, "w+");
			if (fp != NULL)
			{
				nProfilerReportFileHead head;
				head.Initialize(snapshotRef, mModuleReports[id].size());

				fwrite(&head, 1, sizeof(nProfilerReportFileHead), fp);

				uint32 snapShotSize = snapshotRef.GetDataLength();
				char* dataPtr = (char*)( malloc(snapShotSize) );

				for (int i = 0; i < mModuleReports[id].size(); ++i)
				{
					mModuleReports[id][i].WriteData(dataPtr);
					fwrite(dataPtr, 1, snapShotSize, fp);
				}

				fclose(fp);
			}
		}
	}

	nProfileEvent::nProfileEvent(uint16 _id, uint16 _moduleId) : id(_id), moduleId(_moduleId),
		durationInTick(0), callCount(0), lastStartTime(0)
	{

	}

	void nProfileEvent::Reset()
	{
		durationInTick = 0;
		callCount = 0;
	}

	void nProfileEvent::Begin()
	{
		lastStartTime = npTimer::timeGetTime();
	}

	void nProfileEvent::End()
	{
		durationInTick += npTimer::timeGetTime() - lastStartTime;
		callCount++;
	}

	nProfilerSnapshotUnit nProfileEvent::MakeSnapshotUnit()
	{
		nProfilerSnapshotUnit ret;
		ret.callCount = callCount;
		ret.durationInTick = durationInTick;
		ret.eventid = id;

		return ret;
	}

	nProfilerSnapshot::nProfilerSnapshot()
	{
		
	}

	void nProfilerSnapshot::AddEvent(nProfileEvent& event)
	{
		units.push_back(event.MakeSnapshotUnit());
	}

	void nProfilerSnapshot::WriteData(char* dataptr)
	{
		int count = 0;
		for (auto it = units.begin(); it != units.end(); ++it )
		{
			memcpy(dataptr + (count++), &(*it), sizeof(nProfilerSnapshotUnit));
		}
	}

	uint32 nProfilerSnapshot::GetDataLength()
	{
		return units.size() * sizeof(nProfilerSnapshotUnit);
	}

	void nProfilerReportFileHead::Initialize(nProfilerSnapshot& refSnapshot, uint32 count)
	{
		NPRHead = NPRDATA;
		SnapshotCount = count;
		SnapshotUnitCount = refSnapshot.units.size();

		StringDataOffset = FIRSTOFFSET;
		SnapshotDataOffset = FIRSTOFFSET + 0;
	}

}

