#include "interface.h"
#include "memory.h"

static NPR::nProfiler *gProfiler = NULL;

extern "C"
{
    void np_init(const char* datapath)
    {
        gProfiler = new NPR::nProfiler(datapath);
    }
    
    void np_shutdown()
    {
        delete gProfiler;
        gProfiler = NULL;
    }
    
	void np_begin_event(NPR::uint16 id)
	{
		gProfiler->BeginEvent(id);
	}

	void np_end_event(NPR::uint16 id)
	{
		gProfiler->EndEvent(id);
	}

	void np_get_event(NPR::uint16 id, NPR::nProfileEvent& out)
	{
		gProfiler->GetEvent(id, out);
	}

	void np_set_event(NPR::uint16 id, NPR::uint32 timeInTick, NPR::uint32 count)
	{
		gProfiler->SetEvent(id, timeInTick, count);
	}

	void np_begin_report(NPR::uint16 moduleid)
	{
		gProfiler->BeginReport(moduleid);
	}

	void np_end_report(NPR::uint16 moduleid)
	{
		gProfiler->EndReport(moduleid);
	}

	void np_get_report(NPR::uint16 moduleid, char* target, int len)
	{
		gProfiler->GetReportString(moduleid, target, len);
	}

	void np_get_report_by_idx(NPR::uint32 idx, NPR::uint16 moduleid, char* target, int len)
	{
		gProfiler->GetReportStringByIdx(idx, moduleid, target, len);
	}

	void np_get_report_count(NPR::uint16 moduleid, int& count)
	{
		count = gProfiler->GetReportCount(moduleid);
	}

	void np_set_event_name(NPR::uint16 id, NPR::uint16 moduleid, const char* str)
	{
		gProfiler->SetEventName(id, moduleid, str);
	}

	void np_set_module_name(NPR::uint16 id, const char* str)
	{
		gProfiler->SetModuleName(id, str);
	}
    
    int np_get_total_allocate()
    {
        int mem_used = 0;
        int mem_freed = 0;
        
        internal_get_memory(mem_used, mem_freed);
        
        return (int) mem_used;
    }
    
    int np_get_total_free()
    {
        int mem_used = 0;
        int mem_freed = 0;
        
        internal_get_memory(mem_used, mem_freed);
        
        return (int) mem_freed;
    }

}

