#pragma once
#include "nProfiler.h"

extern "C"
{
    // nProfiler interface
    DLL_EXPORT void np_init(const char* datapath);
    DLL_EXPORT void np_shutdown();

	DLL_EXPORT void np_begin_event(NPR::uint16 id);
	DLL_EXPORT void np_end_event(NPR::uint16 id);
	DLL_EXPORT void np_get_event(NPR::uint16 id, NPR::nProfileEvent& out );

	DLL_EXPORT void np_set_event(NPR::uint16 id, NPR::uint32 timeInTick, NPR::uint32 count);

	DLL_EXPORT void np_begin_report(NPR::uint16 moduleid);
	DLL_EXPORT void np_end_report(NPR::uint16 moduleid);

	DLL_EXPORT void np_get_report(NPR::uint16 moduleid, char* target, int len);
	DLL_EXPORT void np_get_report_by_idx(NPR::uint32 idx, NPR::uint16 moduleid, char* target, int len);
	DLL_EXPORT void np_get_report_count(NPR::uint16 moduleid, int& count);

	DLL_EXPORT void np_set_event_name(NPR::uint16 id, NPR::uint16 moduleid, const char* str);
	DLL_EXPORT void np_set_module_name(NPR::uint16 id, const char* str);


    
    // memory usage native access
    DLL_EXPORT int np_get_total_allocate();
    DLL_EXPORT int np_get_total_free();
}