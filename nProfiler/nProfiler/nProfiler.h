//
//  nProfiler.h
//  nProfiler
//
//  Created by gameKnife on 16/3/30.
//  Copyright © 2016年 gameKnife. All rights reserved.
//
#include "platform.h"
#include <vector>
#include <string>

namespace NPR {
    
    struct nProfileEvent;
    
	struct nProfilerSnapshotUnit
	{
		uint16 eventid;
		uint16 callCount;
		uint32 durationInTick;
	};

	struct nProfilerSnapshot
	{
		nProfilerSnapshot();
		void AddEvent(nProfileEvent& event);
		std::vector<nProfilerSnapshotUnit> units;

		void WriteData(char* dataptr);
		uint32 GetDataLength();
	};

#define NPRDATA 0x88332200
#define FIRSTOFFSET 20
	struct nProfilerReportFileHead
	{
		uint32 NPRHead;
		uint32 SnapshotCount;
		uint32 SnapshotUnitCount;
		uint32 StringDataOffset;
		uint32 SnapshotDataOffset;

		void Initialize(nProfilerSnapshot& refSnapshot, uint32 count);
	};

    class nProfiler
    {
    public:
        nProfiler(const char* datapath);
        ~nProfiler();
        
		//////////////////////////////////////////////////////////////////////////
        // profile access
        void BeginEvent(uint16 id);
        void EndEvent(uint16 id);
		void GetEvent(uint16 id, nProfileEvent& out);
		void SetEvent(uint16 id, uint32 timeInTick, uint32 count);
		//////////////////////////////////////////////////////////////////////////
        // report access

		// reset module's events
        void BeginReport(uint16 moduleid);
		// capture event's and make log
        void EndReport(uint16 moduleid);
        
		//////////////////////////////////////////////////////////////////////////
        // visual report
		
		// return log
        void GetReportString(uint16 moduleid, char* target, int len);
		void GetReportStringByIdx(uint32 idx, uint16 moduleid, char* target, int len);
		uint32 GetReportCount(uint16 moduleid);

		//////////////////////////////////////////////////////////////////////////
		// set id to string cache
		void SetEventName(uint16 id, uint16 moduleid, const char* str);
		void SetModuleName(uint16 id, const char* str);
        
		void ExportReportRecord(uint16 moduleid, const char* destAbs);

    private:
        void LogReport(uint16 id);
       
        
    private:
        std::vector<nProfileEvent*> mElements;

		std::vector<std::string> mEventNames;
		std::vector<std::string> mModuleNames;

		std::vector< std::vector<nProfilerSnapshot> > mModuleReports;

		std::string mDataPath;
    };

	struct nProfileEvent
	{
		nProfileEvent(uint16 id, uint16 moduleId);

		uint16 id;
		uint16 moduleId;
        
		uint64_t durationInTick;
		uint64_t lastStartTime;

		uint32 callCount;    
   
		void Reset();
		void Begin();
		void End();

		nProfilerSnapshotUnit MakeSnapshotUnit();
    };
}

extern "C"
{
    
}
