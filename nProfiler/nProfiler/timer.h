#pragma once
#include "platform.h"

#ifdef OS_WIN32
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
#elif defined(OS_IOS)
#include <mach/mach_time.h>

mach_timebase_info_data_t timebaseInfo;
void ProfilerInit()
{
    mach_timebase_info(&timebaseInfo);
}

static float MachToMillisecondsDelta(int64_t delta)
{
    // Convert to nanoseconds
    delta *= timebaseInfo.numer;
    delta /= timebaseInfo.denom;
    float result = (float)delta / 1000000.0F;
    return result;
}
#else

#endif

namespace NPR
{
	struct npTimer
	{
        static void init();
		static uint64_t timeGetTime();
	};
    
    void npTimer::init()
    {
#ifdef OS_WIN32

#elif defined(OS_IOS)
        mach_timebase_info(&timebaseInfo);
#else

#endif
    }

	uint64_t npTimer::timeGetTime()
	{
#ifdef OS_WIN32
		return ::timeGetTime();
#elif defined(OS_IOS)
        return MachToMillisecondsDelta(::mach_absolute_time());
#else
		timespec time;
		clock_gettime(CLOCK_REALTIME, &time);
		return (time.tv_sec * 1000 + time.tv_nsec / 1000000);
#endif
	}

}

