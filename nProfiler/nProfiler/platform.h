#pragma once

#if defined( __SYMBIAN32__ ) 
#   define OS_SYMBIAN
#elif defined( __WIN32__ ) || defined( _WIN32 )
#   define OS_WIN32
#elif defined( __APPLE_CC__)
#   if __ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__ >= 40000 || __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
#       define OS_IOS
#   else
#       define OS_IOS
#       define OS_APPLE
#   endif
#elif defined(__ANDROID__)
#	define OS_ANDROID
#else
#	error "Could not determine OS"
#endif

#if defined (OS_ANDROID) || defined( OS_IOS ) || defined( OS_APPLE )
#	define OS_LINUX
#endif

#if defined( OS_WIN32 )
#include <windows.h>
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __attribute__((visibility("default")))
#endif

namespace NPR
{
#define MAX_UINT16 65535

	typedef unsigned long long uint64_t;

	typedef unsigned int uint32;
	typedef int int32;

	typedef unsigned short uint16;
	typedef short int16;
}