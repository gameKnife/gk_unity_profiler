//
//  memory.h
//  nProfiler
//
//  Created by gameKnife on 16/3/31.
//  Copyright © 2016年 gameKnife. All rights reserved.
//

#ifndef memory_h
#define memory_h

#include "platform.h"

#if defined(OS_WIN32)

#elif defined(OS_IOS)
    #include <mach/mach.h>
#else

#endif

void internal_get_memory(int& used, int& free)
{
#if defined(OS_WIN32)
    
#elif defined(OS_IOS)
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        used = (int)info.resident_size;
    } else {
        used = (int)0;
    }
    
    free = 0;
#else

#endif
}

#endif /* memory_h */
